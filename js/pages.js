//buscar por pages
const pages = {
    'pagina1': {
        title: 'Acesso ao Teia',
        content: '<h2>Acesso ao Teia</h2><p>Recuperação de senha, Login, Como logar, Como se Cadastrar...</p>',
        link: 'acesso.html'
    },
    'pagina2': {
        title: 'Esqueci minha senha',
        content: '<h2>Esqueci minha senha?</h2><p>Recuperação de senha, Login, Como logar, Como se Cadastrar...</p>',
        link: 'acesso.html/'
    },
    'pagina3': {
        title: 'Configurando Teia',
        content: '<h2>Configurando Ambiente</h2><p>Configurando Loja</p><p>Configurando Equipe</p><p>Configurando Perfil de Usuário</p>',
        link: 'config.html'
    },
    'pagina4': {
        title: 'Cadastro de Loja'||'como cadastrar loja?',
        content: '<h2>Configurando Ambiente</h2><p>Configurando Loja</p><p>Configurando Equipe</p><p>Configurando Perfil de Usuário</p>',
        link: 'config.html?id=configurando-loja'
    }

};
