const searchInput = document.getElementById('searchInput');
const searchButton = document.getElementById('searchButton');
const content = document.getElementById('content');

searchButton.addEventListener('click', () => {
    event.preventDefault(); // Impede o comportamento padrão do formulário
    
    const searchTerm = searchInput.value.toLowerCase();
    const matchingPages = [];

    // Verificar se a palavra-chave existe em algum título de página
    for (const pageKey in pages) {
        if (pages.hasOwnProperty(pageKey) && pages[pageKey].title.toLowerCase().includes(searchTerm)) {
            matchingPages.push(pageKey);
        }
    }

    if (matchingPages.length > 0) {
        // Exibir uma lista de páginas correspondentes
        const pageList = matchingPages.map(pageKey => `<a href="${pages[pageKey].link}">${pages[pageKey].title}</a>`).join('<br>');
        content.innerHTML = `<style>#content{display:inline-block;}</style><p class="h5">Resultados da pesquisa: "${searchTerm}"</p><h5>${pageList}</h5>`;
    } else {
        content.innerHTML = `<style>#content{display:inline-block;}</style><p class="h5">Nada por aqui com "${searchTerm}"</p><br/><h4>:(</h4><br/><p class="h5"> Que tal tentar outra palavra?...</p>`;
    }
});