// efeito no sub titulo
const el = document.querySelector("#text");
const text = "Está com dúvidas? Pesquise aqui!";
const interval = 50;

function showText(el, text, interval) {

    const char = text.split("").reverse();

    const typer = setInterval(() =>{

        if(!char.length) {
            return clearInterval(typer);
        }

        const next = char.pop();

        el.innerHTML += next;
        
    }, interval)
}

showText(el, text, interval);


//busca na página inicial
const frequenteText = document.querySelector("#text1");
const filterElement = document.querySelector('header input')
const cards =  document.querySelectorAll('.row .card')

filterElement.addEventListener('input', filterCards)


// filtro de busca
function filterCards() {
    if(filterElement.value != '') {
        
        for (let card of cards) {

            let title = card.querySelector('h5')

            title = title.textContent.toLowerCase()
           
            let filterText = filterElement.value.toLowerCase()

            if(!title.includes(filterText)) {

                card.style.display = "none"
                frequenteText.style.display = "none"
            } 
            else{

                card.style.display = "block"
                frequenteText.style.display = "none"
            }

        }

    } else {

        for (let card of cards) {

            card.style.display = "block"
        }
    }

}

//scroll

const btn = document.getElementById("btnTop")

btn.addEventListener("click", function(){
    window.scrollTo(0,0)
})

document.addEventListener('scroll', Ocultar);

function Ocultar(){
    if(window.scrollY > 10){
        btn.style.display = "flex";
    } else {
        btn.style.display = "none";
    }
}
ocultar();